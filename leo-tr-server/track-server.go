package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

// TrackServer represents tracking server.
type TrackServer struct {
	config    Config // !Interface
	pixel     []byte
	publisher Publisher // !Interface
}

// NewTrackServer creates new TrackServer instance.
func NewTrackServer(config Config, pixel []byte, publisher Publisher) *TrackServer {
	return &TrackServer{
		config:    config,
		pixel:     pixel,
		publisher: publisher,
	}
}

func (ts *TrackServer) rootHandler(writer http.ResponseWriter, request *http.Request) {
	log.Printf("Processing request %v%v", request.Host, request.URL)

	query := request.URL.Query()

	aid, err := strconv.Atoi(query.Get("a"))
	if err != nil {
		code, writeError := writer.Write([]byte(err.Error()))
		if writeError != nil {
			fmt.Printf("writer.Write(%v) code: %v, error: %v", err, code, writeError)
		}

		fmt.Printf("code: %v, error: %v", err, code)

		return
	}

	event := Event{
		ActionID:    aid,
		Profile:     query.Get("p"),
		RedirectURL: query.Get("r"),
	}

	err = ts.publisher.Publish(event)
	if err != nil {
		code, writeError := writer.Write([]byte(err.Error()))
		if writeError != nil {
			fmt.Printf("writer.Write(%v) code: %v, error: %v\n", err, code, writeError)
		}

		fmt.Printf("Publish event: %v error: %v\n", event, err)

		return
	}

	if event.ActionID == ActionPixel {
		writer.Header().Set("Content-Type", "image/png")

		code, writeError := writer.Write(ts.pixel)
		if writeError != nil {
			fmt.Printf("writer.Write(%v) code: %v, error: %v\n", err, code, writeError)
		}

		return
	}

	if event.ActionID == ActionClick || event.ActionID == ActionOpen {
		targetURL, URIerr := url.ParseRequestURI(event.RedirectURL)
		if URIerr != nil {
			code, writeError := writer.Write([]byte(URIerr.Error()))
			if writeError != nil {
				fmt.Printf("writer.Write(%v) code: %v, error: %v\n", URIerr, code, writeError)
			}

			fmt.Println(URIerr)

			return
		}

		http.Redirect(writer, request, targetURL.String(), http.StatusPermanentRedirect)

		fmt.Println(http.StatusPermanentRedirect)
	} else {
		code, writeError := writer.Write([]byte("not valid action parameter"))
		if writeError != nil {
			fmt.Printf("writer.Write(%v) code: %v, error: %v\n", err, code, writeError)
		}

		fmt.Println("not valid action parameter", event)
	}
}

// Start runs tracking server.
func (ts *TrackServer) Start() error {
	http.HandleFunc("/", ts.rootHandler)
	return http.ListenAndServe(ts.config.GetServerAddress(), nil)
}
