package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	jsonconfig "leo-tr-server/json-config"
)

var filePath string
var startTimeOut int

// Application entry point.
func main() {
	flag.StringVar(&filePath, "path", "json-config/config.json", "config path")
	flag.IntVar(&startTimeOut, "time", 0, "service start timeout value")
	flag.Parse()

	time.Sleep(time.Second * time.Duration(startTimeOut))

	if filePath == "" {
		log.Fatalln("not valid path!")
	}

	fmt.Println("start server")
	// Load ConsulConfig
	cfg, err := jsonconfig.NewJSONConfig(filePath)
	if err != nil {
		log.Fatal(err)
	}

	// err := consul.PutValuesToConsul()
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// cfg, err := consul.NewConsulConfig()
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// Prepare pixel
	pixel, err := ioutil.ReadFile(cfg.GetPixelPath())
	if err != nil {
		log.Fatal(err)
	}

	// Prepare publisher
	publisher, err := NewRmqPublisher(cfg.GetRmqURI(), cfg.GetQueueName())
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/favicon.ico", faviconHandler)
	// Start tracking server
	log.Fatal(NewTrackServer(cfg, pixel, publisher).Start())
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "pixel.png")
}
